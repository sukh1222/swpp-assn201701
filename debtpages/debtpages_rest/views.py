from debtpages_rest.models import Debts
from debtpages_rest.serializers import DebtsSerializer,DebtsUpdSerializer,UserSumSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from debtpages_rest.serializers import UserSerializer
from rest_framework import permissions
from debtpages_rest.permissions import PDebtListView,IsDebtAdmin,IsRelatedDebt,IsThatUser


class DebtsList(generics.ListCreateAPIView):
    queryset = Debts.objects.all()
    serializer_class = DebtsSerializer
    permission_classes = (permissions.IsAuthenticated,PDebtListView,)
    def perform_create(self, serializer):
        serializer.save(borrower=self.request.user)


class DebtsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Debts.objects.all()
    serializer_class = DebtsUpdSerializer
    permission_classes = (permissions.IsAuthenticated,IsRelatedDebt,)

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,IsDebtAdmin,)

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,IsThatUser,)

class UserSumList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSumSerializer
    permission_classes = (permissions.IsAuthenticated,IsDebtAdmin,)

class UserSumDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSumSerializer
    permission_classes = (permissions.IsAuthenticated,IsThatUser,)

