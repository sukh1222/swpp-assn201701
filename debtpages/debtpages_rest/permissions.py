from rest_framework import permissions

adminname="debt_admin"


class PDebtListView(permissions.BasePermission):
    def has_permission(self, request, view):
        if adminname == request.user.username:
            return True
        if request.method == "POST":
            return True
        return False

class IsRelatedDebt(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if adminname == request.user.username:
            return True
        if request.method == "POST":
            return obj.borrower == request.user
        if request.method == "DELETE":
            return obj.lender == request.user
        if request.method == "GET":
            return obj.lender == request.user or obj.borrower == request.user
        return False


class IsDebtAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        if adminname == request.user.username:
            return True
        return False

class IsThatUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if adminname == request.user.username:
            return True
        return obj == request.user


