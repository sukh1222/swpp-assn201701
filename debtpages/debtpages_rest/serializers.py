from django.contrib.auth.models import User, Group
from debtpages_rest.models import Debts
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    debts_as_borrower = serializers.PrimaryKeyRelatedField(many=True, queryset=Debts.objects.all())
    debts_as_lender = serializers.PrimaryKeyRelatedField(many=True, queryset=Debts.objects.all())
    class Meta:
        model = User
        fields = ('id', 'username', 'debts_as_borrower', 'debts_as_lender')

class DebtsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Debts
        fields = ('id', 'created', 'amount', 'borrower', 'lender')
    borrower = serializers.ReadOnlyField(source='borrower.id')

class DebtsUpdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Debts
        fields = ('id', 'created', 'amount', 'borrower', 'lender')

class UserSumSerializer(serializers.ModelSerializer):

    lended_money = serializers.SerializerMethodField()
    borrowed_money = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'lended_money', 'borrowed_money')

    def get_lended_money(self, obj):
        sum=0;
        for d in Debts.objects.all():
            if(d.lender==obj):
                sum+=d.amount;
        return sum;

    def get_borrowed_money(self, obj):
        sum=0;
        for d in Debts.objects.all():
            if(d.borrower==obj):
                sum+=d.amount;
        return sum;

