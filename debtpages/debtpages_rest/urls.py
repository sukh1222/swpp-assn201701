from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from debtpages_rest import views
from django.conf.urls import include

urlpatterns = [
    url(r'^debts/$', views.DebtsList.as_view()),
    url(r'^debts/(?P<pk>[0-9]+)/$', views.DebtsDetail.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    url(r'^usersum/$', views.UserSumList.as_view()),
    url(r'^usersum/(?P<pk>[0-9]+)/$', views.UserSumDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
