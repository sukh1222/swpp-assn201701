from rest_framework import permissions

adminname="omok_admin"


class IsOmokAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST":
            if adminname == request.user.username:
                return True
            return False
        return True


