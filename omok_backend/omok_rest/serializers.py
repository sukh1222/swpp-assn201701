from django.contrib.auth.models import User
from omok_rest.models import Room,History
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    room_as_player1 = serializers.PrimaryKeyRelatedField(many=True, queryset=Room.objects.all())
    room_as_player2 = serializers.PrimaryKeyRelatedField(many=True, queryset=Room.objects.all())
    class Meta:
        model = User
        fields = ('id', 'username', 'room_as_player1', 'room_as_player2')

def check_win(room,pl):
    qset=History.objects.filter(room=room,player=pl)
    vect=[[0,1],[1,0],[1,1],[1,-1]];
    board=[]
    for i in range(19):
        row=[False]*19
        board.append(row)
    for q in qset:
        if 0<=q.place_i<19 and 0<=q.place_j<19:
            board[q.place_i][q.place_j]=True
    for v in vect:
        for i in range(19):
            for j in range(19):
                flag=True;
                for k in range(5):
                    if i+k*v[0]>=19 or i+k*v[0]<0 or j+k*v[1]>=19 or j+k*v[1]<0:
                        flag=False;
                        break;
                    if board[i+k*v[0]][j+k*v[1]]==False:
                        flag=False;
                        break;
                if flag:
                    return True
    return False

class RoomSerializer(serializers.ModelSerializer):
    turn = serializers.SerializerMethodField();
    win = serializers.SerializerMethodField();
    class Meta:
        model = Room
        fields = ('id', 'turn', 'win', 'player1', 'player2')
    def get_turn(self, obj):
        tsize = len(History.objects.filter(room=obj))
        return tsize%2+1
    def get_win(self, obj):
        if check_win(obj,obj.player1):
            return 1
        if check_win(obj,obj.player2):
            return 2
        return 0

class PlayerSerializer(serializers.BaseSerializer):
    def to_representation(self,obj):
        l1=([obj.player1.id] if obj.player1 else [])
        l2=([obj.player2.id] if obj.player2 else [])
        return l1+l2
    class Meta:
        model = Room

class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('player','room','place_i','place_j')


