from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from omok_rest import views
from django.conf.urls import include

urlpatterns = [
    url(r'^rooms/$', views.RoomList.as_view()),
    url(r'^rooms/(?P<pk>[0-9]+)/$', views.RoomDetail.as_view()),
    url(r'^rooms/(?P<pk>[0-9]+)/players/$', views.room_players_list),
    url(r'^rooms/(?P<pk>[0-9]+)/history/$', views.room_history_list),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
