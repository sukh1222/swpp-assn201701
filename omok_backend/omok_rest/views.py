from omok_rest.models import Room,History
from omok_rest.serializers import RoomSerializer,UserSerializer,PlayerSerializer,HistorySerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework import status,permissions
from rest_framework.response import Response
from omok_rest.permissions import IsOmokAdminOrReadOnly


class RoomList(generics.ListCreateAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOmokAdminOrReadOnly,)

class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

@api_view(['GET','POST'])
def room_history_list(request,pk):
    try:
        proom=Room.objects.get(pk=pk)
    except Room.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method=='GET':
        qset = History.objects.filter(room=proom)
        serializer=HistorySerializer(qset,many=True)
        return Response(serializer.data)
    elif request.method=='POST':
        turn = len(History.objects.filter(room=proom))%2
        print(request.user)
        print(request.data)
        if turn==0 and proom.player1!=request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)
        if turn==1 and proom.player2!=request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)
        hist=History(player=request.user,room=proom,place_i=request.data["place_i"],place_j=request.data["place_j"])
        hist.save()

@api_view(['GET','POST'])
def room_players_list(request,pk):
    try:
        room=Room.objects.get(pk=pk)
    except Room.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer=PlayerSerializer(room)
    if request.method=='GET':
        return Response(serializer.data)
    elif request.method=='POST':
        if room.player1 == None:
            room.player1=request.user
            room.save()
        elif room.player2 == None:
            room.player2=request.user
            room.save()
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
        return Response(status=status.HTTP_201_CREATED)

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

