import React, { PropTypes } from 'react'
import styled from 'styled-components'
import Block from '../../../components/atoms/Block'
import Button from '../../../components/atoms/Button'
import {initialBoard} from '../../../store/omak/selectors'

const getBlock=(i,j,boardstate,onBoardClick)=>{
    var d;
    for(var k=0;k<boardstate.length;k++){if(boardstate[k].id==i+"_"+j)d=boardstate[k];}
    return (<Block {...d} key={d.id} onClick={()=>onBoardClick(d.id)}/>);
}

const getGame=(boardstate)=>{
    for(var k=0;k<boardstate.length;k++){if(boardstate[k].id=="game")return boardstate[k];}
}

export const Board = ({ boardstate=initialBoard, onBoardClick, onRestart}) => {
  var s=[]
  for(var i=0;i<19;i++){
   var row=[]
   for(var j=0;j<19;j++){
    row=[...row,(<td key={i+"_"+j+"_td"} width="20" height="20">{getBlock(i,j,boardstate,onBoardClick)}</td>)];
   }
   s=[...s,<tr key={i+"_tr"}>{row}</tr>];
  }
  var gstate=getGame(boardstate);
  var labelcontent;
  if(gstate.over)labelcontent=gstate.turn+" Win";
  else labelcontent="Next "+gstate.turn;
  return (
   <div>
    <Button id="restart" type="submit" onClick={onRestart}>RESTART</Button>
    <table>
     <tbody>
      {s}
     </tbody>
    </table>
    <label id="status_label">
     {labelcontent}
    </label>
   </div>
  )
}

Board.propTypes = {
  boardstate:PropTypes.array,
  reverse: PropTypes.bool,
}

