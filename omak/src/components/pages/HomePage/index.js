import React from 'react'

import Board from '../../../containers/Board'

const HomePage = () => {
  return (
    <div>
      <Board/>
    </div>
  )
}

export default HomePage
