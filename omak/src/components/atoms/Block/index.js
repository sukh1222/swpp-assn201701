import React,{ PropTypes } from 'react'
const Block = ({id,onClick,text})=>{
 return <div id={id} onClick={onClick}>{text}</div>
}

Block.propTypes = {
 key:PropTypes.string,
 onClick:PropTypes.func.isRequired,
 text:PropTypes.string,
 reverse:PropTypes.bool
}

export default Block
