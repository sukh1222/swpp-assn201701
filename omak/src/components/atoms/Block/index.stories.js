import React from 'react'
import { storiesOf } from '@kadira/storybook'
import Block from '.'

storiesOf('Block', module)
  .add('default', () => (
    <Block>Hello</Block>
  ))
  .add('reverse', () => (
    <Block reverse>Hello</Block>
  ))
