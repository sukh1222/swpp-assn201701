import {initialBoard} from './selectors'

const putStone=(block,action,turn)=>{
if(block.id!=action.id){return block}
return {...block,text:turn}
}

const mvec=[[0,1],[1,0],[1,1],[1,-1]];

const find_stone=(y,x,boardstate)=>{
    var d;
    for(var k=0;k<boardstate.length;k++){if(boardstate[k].id==y+"_"+x)d=boardstate[k];}
    return d;
}

const gameover=(state,turn)=>{
 for(var vi=0;vi<4;vi++){
  var vy=mvec[vi][0];
  var vx=mvec[vi][1];
  for(var i=0;i<19;i++){
   for(var j=0;j<19;j++){
    var flag=true;
    for(var k=0;k<5;k++){
     var py=i+k*vy;
     var px=j+k*vx;
     if(px<0||px>=19||py<0||py>=19||find_stone(py,px,state).text!=turn){
      flag=false;
      break;
     }
    }
    if(flag)return true;
   }
  }
 }
 return false;
}

function deepcopy(obj){
  if(obj===null||typeof(obj)!='object')return obj;
  console.log(obj);
  var copy=obj.constructor();
  for(var i in obj){
    if(obj.hasOwnProperty(i))copy[i]=deepcopy(obj[i]);
  }
  return copy;
}

const omak_reducer=(state=deepcopy(initialBoard),action)=>{
switch(action.type){
case 'RESTART':
 return deepcopy(initialBoard)
case 'PUT_STONE':
 var turn='-';
 for (var i=0;i<state.length;i++){
  if(state[i].id==action.id&&state[i].text!='-')return state;
  if(state[i].id=='game'){
   turn=state[i].turn;
   if(state[i].over==true)return state;
  }
 }
 var nstate=state.map(t=>putStone(t,action,turn));
 if(gameover(nstate,turn)){
  for (var i=0;i<state.length;i++){
   if(nstate[i].id=='game'){
    nstate[i].over=true;
   }
  }
 }
 else{
  for (var i=0;i<state.length;i++){
   if(nstate[i].id=='game'){
    if(turn=='O')nstate[i].turn='X';
    else nstate[i].turn='O';
   }
  }
 }
 return nstate
default:
 return state
}
}


export default omak_reducer

