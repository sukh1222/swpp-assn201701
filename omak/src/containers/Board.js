import {connect} from 'react-redux'
import {Board} from '../components/molecules/Board'
import {restart,putStone} from '../store/omak/actions'
const mapStateToProps=(state)=>{
 return {boardstate:state.omak}
}


const mapDispatchToProps=(dispatch)=>{
 return {onRestart:()=>{dispatch(restart())},
  onBoardClick:(text)=>{dispatch(putStone(text))}
 }
}

export default connect(mapStateToProps,mapDispatchToProps)(Board)
