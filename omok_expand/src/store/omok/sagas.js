import {delay} from 'redux-saga'
import { take, put, call, fork, spawn } from 'redux-saga/effects'
import api from '../../services/api'
import * as actions from './actions'
import 'whatwg-fetch'
import { stringify } from 'query-string'
import merge from 'lodash/merge'
import { apiUrl } from 'config'


const url = 'http://127.0.0.1:8000/'

function players_url(rid){
    return url+"rooms/"+rid+"/players/";
}

function room_url(rid){
    return url+"rooms/"+rid+"/";
}

function history_url(rid){
    return url+"rooms/"+rid+"/history/";
}

function user_url(uid){
    return url+"users/"+uid+"/";
}

var hash;

export function* postEnterRoom(data) {
    let uname=data.uname
    let upwd=data.upwd
    let rid=data.rid
    console.log("Logging in...");

    hash=new Buffer(`${uname}:${upwd}`).toString('base64')
    
    const response=yield call(fetch,players_url(rid),{method:'POST',headers:{'Authorization':`Basic ${hash}`}})
    console.log(response);
    if(!response.ok){
        console.log("Logging in Failed.");
        yield call(watchLogin);
    }
    else{
        yield put(actions.enterRoomSuccess(rid,uname));
    }
}

export function* watchLogin(){
    const data=yield take('POST_ENTER_ROOM_REQUEST')
    console.log(data)
    yield call(postEnterRoom,data)
}

export function* createEnemyWait(){
    const data=yield take('ENTER_ROOM_SUCCESS')
    yield call(acceptEnemy,data)
}

export function* createNewAction(data){
    console.log("createNewAction spawned")
    const rid=data.rid
    const uname=data.uname
    console.log("Waiting enemy...")
    yield delay(1000)
    console.log("1s passed")
    yield put(actions.getAcceptEnemyRequest(rid,uname))
}

export function* acceptEnemy(data){
    let rid=data.rid
    let uname=data.uname

    console.log("Getting player list")
    const res=yield call(api.get,players_url(rid))

    let usernames=[]
    for(var i=0;i<res.length;i++){
        const ires=yield call(api.get,user_url(res[i]))
        usernames.push(ires["username"]);
    }

    console.log(usernames)

    if(usernames.length==2){
        let enemyname=""
        let myturn=0
        if(usernames[0]==uname){
            enemyname=usernames[1]
            myturn=1
        }
        else{
            enemyname=usernames[0]
            myturn=2
        }
        yield put(actions.startGame(enemyname,myturn))
        yield spawn(createRefreshreq,rid)
        yield spawn(boardListener,rid,myturn)
    }
    else{
        console.log("no enemy")
        yield spawn(createNewAction,{rid,uname})
    }
}

export function* waitEnemy(){
    while(true){
        console.log("Waiting for GET_ACCEPT_ENEMY_REQUEST...")
        const data=yield take('GET_ACCEPT_ENEMY_REQUEST')
        yield call(acceptEnemy,data)
    }
}

export function* createRefreshreq(rid){
    console.log("next refresh in 0.9s")
    yield delay(900)
    console.log("0.9s passed")
    yield put(actions.getRefreshRequest(rid))
}

const histmap=(t,player1,player2)=>{
  var p='-'
  if(t.player==player1)p='O'
  else p='X'
  return {place_i:t.place_i,place_j:t.place_j,text:p}
}

export function* seeHistory(rid){
    const data=yield call(api.get,history_url(rid))
    const gamed=yield call(api.get,room_url(rid))
    console.log(data)
    console.log(gamed)
    const winner=gamed["win"]
    const turn=gamed["turn"]
    yield put(actions.refreshBoard(winner,turn,data.map(t=>histmap(t,gamed["player1"],gamed["player2"]))))
}

export function* autoRefresh(){
    while(true){
        const data=yield take('GET_REFRESH_REQUEST')
        const rid=data.rid
        yield call(seeHistory,rid)
        yield spawn(createRefreshreq,rid)
    }
}

export function* boardListener(rid,myturn){
    while(true){
        const data=yield take('PUT_STONE')
        const loc=data.id.split("_")
        const hdata=yield call(api.get,history_url(rid))
        const gdata=yield call(api.get,room_url(rid))
        if(gdata["win"]!=0)continue;
        if(gdata["turn"]!=myturn)continue;
        var flag=0;
        for(var i=0;i<hdata.length;i++){
            if(hdata[i].place_i==loc[0]&&hdata[i].place_j==loc[1]){
                flag=1;
                break;
            }
        }
        if(flag!=0)continue;
        const content="{\"place_i\":"+loc[0]+",\"place_j\":"+loc[1]+"}"
        const resp=yield call(fetch,history_url(rid),{method:'POST',headers:{'Authorization':`Basic ${hash}`,'Content-Type':'application/json'},body:content})
        console.log(resp)
        yield call(seeHistory,rid)
    }
}

export default function* () {
    yield fork(watchLogin)
    yield fork(createEnemyWait)
    yield fork(waitEnemy)
    yield fork(autoRefresh)
}
