import {initialBoard} from './selectors'

const enterRoom=(t,rid)=>{
  if(t.id!="game"){return t}
  return {...t,room:rid,loggedin:true}
}

const startGame=(t,en,mturn)=>{
  if(t.id!="game"){return t}
  return {...t,enemyName:en,myturn:mturn,started:true}
}

const refrmap=(t,w,tn,darr)=>{
  if(t.id=="game"){
    return {...t,turn:tn,winner:w}
  }
  var p=t.id.split('_')
  return {...t,text:darr[p[0]][p[1]]}
}

const putStone=(block,action,turn)=>{
if(block.id!=action.id){return block}
return {...block,text:turn}
}

const mvec=[[0,1],[1,0],[1,1],[1,-1]];

const find_stone=(y,x,boardstate)=>{
    var d;
    for(var k=0;k<boardstate.length;k++){if(boardstate[k].id==y+"_"+x)d=boardstate[k];}
    return d;
}

function deepcopy(obj){
  if(obj===null||typeof(obj)!='object')return obj;
  var copy=obj.constructor();
  for(var i in obj){
    if(obj.hasOwnProperty(i))copy[i]=deepcopy(obj[i]);
  }
  return copy;
}

const omok_reducer=(state=deepcopy(initialBoard),action)=>{
console.log(action)
switch(action.type){
case 'ENTER_ROOM_SUCCESS':
 return state.map(t=>enterRoom(t,action.rid));
case 'START_GAME':
 return state.map(t=>startGame(t,action.enemyname,action.myturn));
case 'REFRESH_BOARD':
 console.log(action.data)
 var arr=[]
 for(var i=0;i<19;i++){
  var row=[]
  for(var j=0;j<19;j++){
   row.push('-')
  }
  arr.push(row)
 }
 for(var i=0;i<action.data.length;i++){
  arr[action.data[i].place_i][action.data[i].place_j]=action.data[i].text
 }
 return state.map(t=>refrmap(t,action.winner,action.turn,arr));
default:
 return state
}
}


export default omok_reducer

