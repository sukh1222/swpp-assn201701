export const putStone=(id)=>{
 return{
  type:'PUT_STONE',
  id
 }
}

export const tryLogin=(uname,upwd,rid)=>{
 return{
  type:'POST_ENTER_ROOM_REQUEST',
  uname,upwd,rid
 }
}

export const enterRoomSuccess=(rid,uname)=>{
 return{
  type:'ENTER_ROOM_SUCCESS',
  rid,
  uname
 }
}

export const getAcceptEnemyRequest=(rid,uname)=>{
 return{
  type:'GET_ACCEPT_ENEMY_REQUEST',
  rid,
  uname
 }
}

export const startGame=(enemyname,myturn)=>{
 return{
  type:'START_GAME',
  enemyname,
  myturn
 }
}

export const getRefreshRequest=(rid)=>{
 return{
  type:'GET_REFRESH_REQUEST',
  rid
 }
}

export const refreshBoard=(winner,turn,data)=>{
 return{
  type:'REFRESH_BOARD',
  winner,
  turn,
  data
 }
}


