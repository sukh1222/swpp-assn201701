import {connect} from 'react-redux'
import {Board} from '../components/molecules/Board'
import {tryLogin,putStone} from '../store/omok/actions'
const mapStateToProps=(state)=>{
 return {boardstate:state.omok}
}


const mapDispatchToProps=(dispatch)=>{
 return {onConnect:(uname,upwd,rid)=>{dispatch(tryLogin(uname,upwd,rid))},
  onBoardClick:(text)=>{dispatch(putStone(text))}
 }
}

export default connect(mapStateToProps,mapDispatchToProps)(Board)
