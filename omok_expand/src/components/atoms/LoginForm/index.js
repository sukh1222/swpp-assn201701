import React,{ PropTypes } from 'react'
import Button from '../../../components/atoms/Button'
export const LoginForm = ({enemyName,onConnect})=>{
   var upwd;
   var rid;
   return(
    <div>
     <p>
      Login : 
      <input id="username_field" type="text"/>
     </p>
     <p>
     PW : 
      <input id="password_field" type="password"/>
     </p>
     <p>
      Room Number : 
      <input id="room_field" type="text"/>
     </p>
     <p>
      Enemy : 
      <label id="enemy_field">
       {enemyName}
      </label>
     </p>
     <Button id="connect" type="submit" onClick={()=>onConnect(document.getElementById("username_field").value,document.getElementById("password_field").value,document.getElementById("room_field").value)}>Connect</Button>
    </div>)
}

LoginForm.propTypes = {
 enemyName:PropTypes.string,
 onConnect:PropTypes.func.isRequired,
 reverse:PropTypes.bool
}

export default LoginForm
