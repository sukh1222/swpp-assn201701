import React, { PropTypes } from 'react'
import styled from 'styled-components'
import Block from '../../../components/atoms/Block'
import LoginForm from '../../../components/atoms/LoginForm'
import {initialBoard} from '../../../store/omok/selectors'

const getBlock=(i,j,boardstate,onBoardClick)=>{
    var d;
    for(var k=0;k<boardstate.length;k++){if(boardstate[k].id==i+"_"+j)d=boardstate[k];}
    return (<Block {...d} key={d.id} onClick={()=>onBoardClick(d.id)}/>);
}

const getGame=(boardstate)=>{
    for(var k=0;k<boardstate.length;k++){if(boardstate[k].id=="game")return boardstate[k];}
}

export const Board = ({ boardstate=initialBoard, onBoardClick, onConnect}) => {
  var s=[]
  for(var i=0;i<19;i++){
   var row=[]
   for(var j=0;j<19;j++){
    row=[...row,(<td key={i+"_"+j+"_td"} width="20" height="20">{getBlock(i,j,boardstate,onBoardClick)}</td>)];
   }
   s=[...s,<tr key={i+"_tr"}>{row}</tr>];
  }
  var gstate=getGame(boardstate);
  var ename=gstate.enemyName;
  var labelcontent;
  if(gstate.loggedin==false)labelcontent="";
  else if(gstate.started==false)labelcontent="WAITING";
  else if(gstate.winner==0){
    if(gstate.turn==1){
      labelcontent="Next O"
    }
    else{
      labelcontent="Next X"
    }
  }
  else{
    if(gstate.winner==1){
      labelcontent="O win"
    }
    else{
      labelcontent="X win"
    }
  }
  return (
   <div>
    <LoginForm enemyName={ename} onConnect={(uname,upwd,rid)=>onConnect(uname,upwd,rid)}/>
    <table>
     <tbody>
      {s}
     </tbody>
    </table>
    <label id="status_label">
     {labelcontent}
    </label>
   </div>
  )
}

Board.propTypes = {
  boardstate:PropTypes.array,
  reverse: PropTypes.bool,
}

